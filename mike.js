// importar dependencias
require("dotenv").config();
const OpenAI = require("openai");
const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Crea la conexión con openai
const secretKey = process.env.OPENAI_API_KEY;
const openai = new OpenAI({
  apiKey: secretKey,
});

async function askQuestion(question) {
  return new Promise((resolve, reject) => {
    readline.question(question, (answer) => {
      resolve(answer);
    });
  });
}

async function main() {
  try {
    const assistant = await openai.beta.assistants.create({
      name: "nombre de tu personaje",
      instructions:"escribe tus instrucciones",
      tools: [{ type: "code_interpreter" }],
      model: "gpt-4-1106-preview",
    });

    // Guarda el primer mensaje
    console.log(
      "\nsaludo de tu personaje.\n"
    );

    // Creat el hilo de conversación
    const thread = await openai.beta.threads.create();

    // Usa keepasking para continuar preguntando
    let keepAsking = true;
    while (keepAsking) {
      const userQuestion = await askQuestion("\nTienes alguna duda? ");

      // Pasa la pregunta al hilo actual de conversación
      await openai.beta.threads.messages.create(thread.id, {
        role: "user",
        content: userQuestion,
      });

      // Espera la respuesta del asistente
      const run = await openai.beta.threads.runs.create(thread.id, {
        assistant_id: assistant.id,
      });

      let runStatus = await openai.beta.threads.runs.retrieve(
        thread.id,
        run.id
      );

      // Mecanismo de sondeo para ver si runStatus está completo
      // Esta parte está incompleta y debe ser más robusta
      while (runStatus.status !== "completed") {
        await new Promise((resolve) => setTimeout(resolve, 2000));
        runStatus = await openai.beta.threads.runs.retrieve(thread.id, run.id);
      }
      // Obtiene el último mensaje del asistente de la matriz de mensajes
      const messages = await openai.beta.threads.messages.list(thread.id);

      // Encuentra el ultimo mensaje en esta ejecución
      const lastMessageForRun = messages.data
        .filter(
          (message) => message.run_id === run.id && message.role === "assistant"
        )
        .pop();

      // Si encuentra alguno, console.log() 
      if (lastMessageForRun) {
        console.log(`${lastMessageForRun.content[0].text.value} \n`);
      }
      
      // Luego pregunta si el usuario quiere hacer otra pregunta y actualiza el estado keepAsking
      const continueAsking = await askQuestion(
        "Quieres preguntar algo más? (si/no) "
      );
      keepAsking = continueAsking.toLowerCase() === "si";
      
      // Si el estado keepAsking es falso, muestra un mensaje final
      if (!keepAsking) {
        console.log("Bien, espero haberte ayudado, y estoy a tu disposición cuando me necesites!\n");
      }
    }

    // cierra la linea de lectura
    readline.close();
  } catch (error) {
    console.error(error);
  }
}

// Llamada a la función principal
main();