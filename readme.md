# Asistente Chatbot Usando OpenAI

Este es un chatbot de consola simple desarrollado en Node.js que usa la API de OpenAI para conversar con el usuario.

## Características
- Convierte preguntas del usuario en mensajes codificados que son enviados a OpenAI.
- Espera una respuesta de OpenAI y la muestra al usuario.
- La interacción continúa hasta que el usuario decida terminar la conversación.

## Requisitos
Necesitarás Node.js y npm instalados en tu computadora para ejecutar este proyecto.

## Instalación

Para poner en marcha el asistente chatbot, sigue estos pasos:

### 1. Clona el repositorio
Clona el repositorio a tu máquina local y navega hasta el directorio del proyecto.

`git clone <URL del repositorio>
cd <nombre del directorio del proyecto>`


### 2. Instala las dependencias
Utiliza el siguiente comando para instalar las dependencias necesarias para el proyecto.

`npm install`

### 3. Configura la clave de la API
Crea un archivo `.env` en la raíz del proyecto y establece la clave de la API de OpenAI.

`OPENAI_API_KEY=<Tu clave de la API de OpenAI aquí>`


## Uso

Para iniciar el chatbot, simplemente ejecuta el script principal:

`node Mike.js`


Cuando se te pregunte algo, escribe tu pregunta y presiona Enter. El asistente responderá a tus preguntas.

Para finalizar la conversación, cuando se te pregunte si quieres hacer otra pregunta, simplemente responde "no", y el chatbot te despedirá.
